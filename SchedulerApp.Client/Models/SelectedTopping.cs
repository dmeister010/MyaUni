﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchedulerApp.Client.Models
{
    /* Table of the selected toppings for an order*/
    public class SelectedTopping
    {
        public int ID { get; set; }   // primary key

        public int OrderID { get; set; }
        public int ToppingID { get; set; }

        public virtual Order Order { get; set; }
        public virtual Topping Topping { get; set; }
    }
}