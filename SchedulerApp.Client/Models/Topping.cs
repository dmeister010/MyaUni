﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchedulerApp.Client.Models
{
    public class Topping
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public bool IsSelected { get; set; }

        public virtual ICollection<SelectedTopping> SelectedToppings { get; set; }
    }
}