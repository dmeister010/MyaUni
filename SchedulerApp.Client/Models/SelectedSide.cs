﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchedulerApp.Client.Models
{
    public class SelectedSide
    {
        public int ID { get; set; }   // primary key

        public int OrderID { get; set; }
        public int SideID { get; set; }

        public virtual Order Order { get; set; }
        public virtual Side Side { get; set; }
    }
}