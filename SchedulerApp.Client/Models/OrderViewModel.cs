﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchedulerApp.Client.Models
{
    public class OrderViewModel
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public int SizeID { get; set; }
        public int[] SelectedToppingIDs { get; set; }
        public IEnumerable<Topping> Toppings { get; set; }
    }
}