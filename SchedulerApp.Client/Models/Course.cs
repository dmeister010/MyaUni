﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerApp.Client.Models
{
    /* ***********************************
     * Database relationship:
     *      Course-Enrollment: one to many
     * *********************************** */
    public class Course
    {
        /* Let user enter primary key for this course */
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        /* ********************************************* 
         * entity that are named 'ID' or '<classname>ID' 
         * are regconized as primary key 
         * ********************************************* */
        public int CourseID { get; set; }     // primary key
        public string Title { get; set; }
        public int Credits { get; set; }

        /* Navigation property. See Student.cs for details */
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}