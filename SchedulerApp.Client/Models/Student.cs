﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchedulerApp.Client.Models
{
    /* ************************************
     * Database relationship:
     *      Student-Enrollment: one to many
     * ************************************ */
    public class Student
    {
        public int ID { get; set; }     // primary key
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime EnrollmentDate { get; set; }

        /* ********************************************************************
         * Navigation property: contain multiple entities related to this class.
         * Icollection is used instead of List because it's more compatible
         * with SQL database.
         * Virtual: allow this property to be overridden in a derived class.
         * ******************************************************************** */
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}