﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/* ***********************************
 * Database relationship:
 *      Enrollment-Student: one to one
 *      Enrollment-Course: one to one  
 * *********************************** */
namespace SchedulerApp.Client.Models
{
    public enum Grade
    {
        A, B, C, D, F
    }

    public class Enrollment
    {
        public int ID { get; set; }   // primary key

        /* ******************************************************************
         * Foreign keys:
         *  Entity Framework knows if a property is a foreign when name
         *  is formated <navigation property name><primary key property name>
         *  or <primary key property name>
         * ****************************************************************** */
        public int CourseID { get; set; }       // correspond to course
        public int StudentID { get; set; }      // correspond to student
            
        public Grade? Grade { get; set; }   // ? mean it can be null

        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }
    }
}