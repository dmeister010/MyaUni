﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchedulerApp.Client.Models
{
    public class Order
    {
        public int ID { get; set; }

        public int CustomerID { get; set; }
        public int SizeID { get; set; }

        //public double Total { get; set; }
        //public int OrderToppingID { get; set; }
        //public MultiSelectList Toppings { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Size Size { get; set; }
        public virtual ICollection<SelectedTopping> SelectedToppings { get; set; }
        public virtual ICollection<SelectedSide> SelectedSides { get; set; }
    }
}