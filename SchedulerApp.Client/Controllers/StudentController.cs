﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchedulerApp.Client.Data;
using SchedulerApp.Client.Models;
using PagedList;        // required PagedList.mvc nuget package

namespace SchedulerApp.Client.Controllers
{
    public class StudentController : Controller
    {
        private SchoolContext db = new SchoolContext();

        /* ****************************************************************
         * sortOrder and searchString is the query string in the URL.
         * Each time this is called, ViewBag.NameSortParm toggle
         * between ascending and descending. Then in View, sortOrder is set 
         * with ViewBag.NameSortParm and passed back here. 
         * Added Pagedlist: changed return type from ActionResult to ViewResult
         * **************************************************************** */
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            /* ViewBag is used to transfer temporary data between controller and view */
            ViewBag.CurrentSort = sortOrder;        // keep same sortOrder when changing page
            ViewBag.LNameSortParm = String.IsNullOrEmpty(sortOrder) ? "lname_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.FNameSortParm = sortOrder == "fname" ? "fname_desc" : "fname";

            /* If searchString is not null, a new list need to be displayed, starting from page 1 */
            if (searchString != null)
            {
                page = 1;
            }

            else
            {
                searchString = currentFilter;
            }

            /* ****************************************************
             * ViewBag.CurrentFilter hold the current filter string 
             * to maintain filtered results whenchanging page
             * **************************************************** */
            ViewBag.CurrentFilter = searchString;

            /* ********************************************
             * Create IQueryable varialbe. This is not sent 
             * to the database until ToList is called 
             * ******************************************** */
            var students = from s in db.Students select s;      // LINQ to Entities 
            // only display filtered result if searchString was specified
            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.LastName.Contains(searchString)
                                            || s.FirstName.Contains(searchString));
            }

            /* Order by FirstName, LastName or by Date. Default is Ascending LastName */
            switch (sortOrder)
            {
                case "lname_desc":
                    students = students.OrderByDescending(s => s.LastName);
                    break;
                case "Date":
                    students = students.OrderBy(s => s.EnrollmentDate);
                    break;
                case "date_desc":
                    students = students.OrderByDescending(s => s.EnrollmentDate);
                    break;
                case "fname":
                    students = students.OrderBy(s => s.FirstName);
                    break;
                case "fname_desc":
                    students = students.OrderByDescending(s => s.FirstName);
                    break;
                default:
                    students = students.OrderBy(s => s.LastName);
                    break;
            }

            int pageSize = 5;           // 5 rows per page
            int pageNumber = (page ?? 1);       // if page param is null, set to 1
            /* Changed this line to return the new list of sorted students */
            //return View(db.Students.ToList());        // all students
            //return View(students.ToList());       // added filter
            return View(students.ToPagedList(pageNumber, pageSize));        // added paging
        }

        // GET: Student/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LastName,FirstName,EnrollmentDate")] Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Students.Add(student);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            
            catch (DataException /* dex */)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again later.");
            }

            return View(student);
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /* reference this method to action Edit. Required if name of method is changed from
         * Edit to something else */
        [HttpPost, ActionName("Edit")] 
        [ValidateAntiForgeryToken]
        /* have to change the name of method beacuse it has the same signature as the 
         * HttpGet method. But this is still call when "Edit" is requested because
         * it is referenced above 
         */
        public ActionResult EditPost(int? id)       
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            /* update with fields from user input */
            var studentToUpdate = db.Students.Find(id);
            if (TryUpdateModel(studentToUpdate, "", 
                new string[] { "Lastname", "FirstName", "EnrollmentDate" }) )
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DataException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again.");
                }
            }
            return View(studentToUpdate);
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError=false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            /* if there an error, httpPost delete will set saveChangesError to true */
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage("Delete failed. Try again.");
            }

            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Student student = db.Students.Find(id);
                db.Students.Remove(student);
                db.SaveChanges();
            }
            
            catch (DataException /* dex */)
            {
                //go back to httpGet Delete with new params
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
