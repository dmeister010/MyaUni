﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchedulerApp.Client.Data;
using SchedulerApp.Client.Models;

namespace SchedulerApp.Client.Controllers
{
    public class OrderController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: Order
        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.Customer).Include(o => o.Size);
            return View(orders.ToList());
        }

        // GET: Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName");
            ViewBag.SizeID = new SelectList(db.Sizes, "ID", "Name");
            ViewBag.ToppingList = new SelectList(db.Toppings, "ID", "Name");
            ViewBag.ToppingCount = db.Toppings.Count();
            //MultiSelectList ToppingList = new MultiSelectList(db.Toppings.ToList(), "ID", "Name");
            //Order order = new Order { Toppings = ToppingList };

            //List<SelectListItem> listSelectListItems = new List<SelectListItem>();
            //foreach (Topping topping in db.Toppings)
            //{
            //    SelectListItem selectList = new SelectListItem()
            //    {
            //        Text = topping.Name,
            //        Value = topping.ID.ToString(),
            //        Selected = topping.IsSelected,
            //    };
            //    listSelectListItems.Add(selectList);
            //}

            //OrderViewModel orderView = new OrderViewModel()
            //{
            //    Toppings = db.Toppings,
            //    //SelectedToppings = new List<SelectedTopping>(),
            //};

            //orderView.SelectedToppings.Add("Ham");

            return View();
        }

        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,CustomerID,SizeID")] Order order)
        public ActionResult Create([Bind(Include = "ID,CustomerID,SizeID")] Order order, string[] ToppingIDs)
        {
            if (ModelState.IsValid)
            {
                if (ToppingIDs != null)
                {
                    //Order newOrder = new Order { CustomerID = 1, SizeID = 1 };
                    db.Orders.Add(order);
                    db.SaveChanges();
                    foreach (string toppingID in ToppingIDs)
                    {
                        db.SelectedToppings.Add(new SelectedTopping { OrderID = order.ID, ToppingID = Int32.Parse(toppingID)});
                    }

                    //foreach (var toppingName in order.SelectedToppings)
                    //{
                    //    IEnumerable<Topping> toppings = from t in db.Toppings select t;
                    //    toppings = toppings.Where(t => t.Name.Contains(toppingName));
                    //    foreach (Topping topping in toppings)
                    //    {
                    //        db.SelectedToppings.Add(new SelectedTopping { OrderID = order.ID, ToppingID = topping.ID });
                    //    }
                        //Topping topping = db.Toppings.Find(sTopping.ToppingID);

                    //}

                    // TODO: collect order.CustomerID and order.SizeID in view
                    
                    db.SaveChanges();
                }
                
                
                return RedirectToAction("Index");
            }

            ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", order.CustomerID);
            ViewBag.SizeID = new SelectList(db.Sizes, "ID", "Name", order.SizeID);
            return View(order);
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", order.CustomerID);
            ViewBag.SizeID = new SelectList(db.Sizes, "ID", "Name", order.SizeID);
            return View(order);
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CustomerID,SizeID")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", order.CustomerID);
            ViewBag.SizeID = new SelectList(db.Sizes, "ID", "Name", order.SizeID);
            return View(order);
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
