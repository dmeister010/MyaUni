﻿using SchedulerApp.Client.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace SchedulerApp.Client.Data
{
    /* A context is equivalent to a database */
    public class SchoolContext: DbContext
    {
        /* Name of connection string which will be added to Web.config later */
        public SchoolContext() : base("SchoolContext") {}

        /* ***********************************************************
         * An DbSet is equivalent to a database table 
         * Each entity (eg. student) corresponds to a row in the table
         * *********************************************************** */ 
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Topping> Toppings { get; set; }
        public DbSet<Side> Sides { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<SelectedTopping> SelectedToppings { get; set; }
        public DbSet<SelectedSide> SelectedSides { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /* ****************************************
             * Prevent table name from being pluralized 
             * So Students will become Student instead 
             * **************************************** */
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}