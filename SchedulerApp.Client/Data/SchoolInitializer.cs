﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SchedulerApp.Client.Models;

namespace SchedulerApp.Client.Data
{
    /* *************************************************
     * Tell Entity Framework to Drop a database when the 
     * models has changed and create a new one.
     * ************************************************* */
    public class SchoolInitializer : DropCreateDatabaseIfModelChanges<SchoolContext>
    {
        /* *******************************************************************
         * Seed is called after database has been created to populate it data.
         * Take database context as input and add new entities to it 
         * ******************************************************************* */
        protected override void Seed(SchoolContext context)
        {
            
            var students = new List<Student>
            {
                new Student { FirstName="Mya", LastName="Pham",EnrollmentDate=DateTime.Parse("2017-05-01")},
                new Student { FirstName="Majin", LastName="Buu",EnrollmentDate=DateTime.Parse("1998-11-28")},
                new Student { FirstName="Lionel", LastName="Messi", EnrollmentDate=DateTime.Now},
                new Student { FirstName="Alexander", LastName="Kun", EnrollmentDate=DateTime.Now},
                new Student { FirstName="Helga", LastName="Mono", EnrollmentDate=DateTime.Now},
            };

            var courses = new List<Course>
            {
                new Course {CourseID=1245, Title="Calculus", Credits=3 },
                new Course {CourseID=1364, Title="Physics", Credits=3 },
                new Course {CourseID=1135, Title="Chemistry", Credits=3 },
                new Course {CourseID=3842, Title="English", Credits=3 },
                new Course {CourseID=4721, Title="Computer Science", Credits=3 },
            };

            var enrollments = new List<Enrollment>
            {
                new Enrollment {StudentID=1, CourseID=1245, Grade=Grade.A},
                new Enrollment {StudentID=1, CourseID=3842, Grade=Grade.A},
                new Enrollment {StudentID=1, CourseID=4721, Grade=Grade.A},
                new Enrollment {StudentID=2, CourseID=1364, Grade=Grade.F},
                new Enrollment {StudentID=2, CourseID=1135, Grade=Grade.F},
                new Enrollment {StudentID=3, CourseID=1245},
                new Enrollment {StudentID=3, CourseID=1364},
                new Enrollment {StudentID=4, CourseID=4721},
                new Enrollment {StudentID=4, CourseID=3842},
                new Enrollment {StudentID=5, CourseID=1135},
                new Enrollment {StudentID=5, CourseID=4721},
            };

            /* Add all the created entities to their DbSet */
            students.ForEach(s => context.Students.Add(s));
            context.SaveChanges();
            courses.ForEach(c => context.Courses.Add(c));
            context.SaveChanges();
            enrollments.ForEach(e => context.Enrollments.Add(e));
            context.SaveChanges();

            var customers = new List<Customer>
            {
                new Customer { FirstName="Mya", LastName="Pham"},
                new Customer { FirstName="Majin", LastName="Buu"},
                new Customer { FirstName="Lionel", LastName="Messi"},
                new Customer { FirstName="Alexander", LastName="Kun"},
                new Customer { FirstName="Helga", LastName="Mono"},

            };

            customers.ForEach(s => context.Customers.Add(s));
            context.SaveChanges();

            var sizes = new List<Size>
            {
                new Size {Name="Small", Price=6.99},
                new Size {Name="Medium", Price=8.99},
                new Size {Name="Large", Price=10.99},
                new Size {Name="Extra Large", Price=12.99},
            };

            sizes.ForEach(s => context.Sizes.Add(s));
            context.SaveChanges();

            var toppings = new List<Topping>
            {
                new Topping {Name="Pepperoni", Price=0.99},
                new Topping {Name="Beef", Price=0.99},
                new Topping {Name="Ham", Price=0.99},
                new Topping {Name="Sausage", Price=0.99},
                new Topping {Name="Extra Cheese", Price=0.99},
                new Topping {Name="Olive", Price=0.99},
                new Topping {Name="Tomato", Price=0.99},
                new Topping {Name="Pineapple", Price=0.99},
            };

            toppings.ForEach(s => context.Toppings.Add(s));
            context.SaveChanges();

            Order order = new Order { CustomerID = 1, SizeID = 1 };
            context.Orders.Add(order);
            SelectedTopping oneTopping = new SelectedTopping { OrderID = 1, ToppingID = 1 };
            context.SelectedToppings.Add(oneTopping);

            context.SaveChanges();


            var sides = new List<Side>
            {
                new Side {Name="Garlic Breads", Price=4.99},
                new Side {Name="Chicken Wings", Price=6.99},
                new Side {Name="Cheese Sticks", Price=12.99},
            };

            sides.ForEach(s => context.Sides.Add(s));
            context.SaveChanges();

            SelectedSide oneSide = new SelectedSide { OrderID = 1, SideID = 1 };
            context.SelectedSides.Add(oneSide);
            context.SaveChanges();

        }
    }
}