﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;     // make sure this is added in Reference

namespace Scheduler.App.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("ADO.NET with localDb\n");
            DataTable table = new DataTable();
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Data Source=(LocalDb)\\ProjectsV13;Initial Catalog=SchedulerApp.Storage;"].ConnectionString);
            SqlConnection con = new SqlConnection("Data Source=(LocalDb)\\ProjectsV13;Initial Catalog=SchedulerApp.Storage;");

            // when cmd and conn is specified, adapter will automatically open and close connection
            SqlDataAdapter da = new SqlDataAdapter("select ID, FirstName, LastName from Student", con);
            da.Fill(table);    // run the select query and copy into DataTable

            int id;
            System.Console.WriteLine("{0, -2} {1, -10} {2, -10}", "ID", "FirstName", "LastName");
            foreach (DataRow row in table.Rows)
            {
                id = int.Parse(row["ID"].ToString());
                System.Console.WriteLine("{0, -2} {1, -10} {2, -10}", id, row["FirstName"], row["LastName"]);
            }
            System.Console.WriteLine();

            //try
            //{
            //    da.Fill(table);
            //    cmd.ExecuteNonQuery();
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //finally
            //{
            //    cmd.Dispose();
            //    con.Close();
            //}
        }
    }
}
